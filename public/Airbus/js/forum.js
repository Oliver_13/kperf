function last_topic(result) {
    setTimeout(function() {
        var item = $('.forum-items').find('.forum-item').first();
        item.find('a').attr('href', result.topic.url+result.topic.nav_url);
        item.removeClass('hidden');
        setTimeout(function() {
            $('.new_topic').fadeOut(500, function() {
                $(this).addClass('hidden');
            }).end().find('div.content').text('');
            $('.new_topic').find('.messages').remove();
            $('.alert-warning').remove();
        }, 1500);
    }, 500);
}

$(function() {

    var select_visibility = $('#select_visibility'), li = select_visibility.find('li');
    select_visibility.find('i').click(function(e) {
        e.stopPropagation();
        li.show();
        li.click(function() {
            li.not(this).hide();
        });
    });

    $('#forum_create_topic').click(function() {
        var topic = $(this).next('.new_topic'), title = topic.find('div.content:first-of-type');
        topic.removeClass('hidden').show();
        title.focus();
        topic.find('button').click(function() {
            var topic_title = title.text();
            var topic_message = topic.find('div.content.text');
            $.when(ajax('ajax_object=forum&ajax_action=create_topic&visibility='+(select_visibility.find('li:visible').attr('data-level'))+'&topic_title='+topic_title+'&topic_message='+(topic_message.text())+'&callback=last_topic', $(this).closest('.form')))
            .then(function() {
                $('.forum-items').prepend($('template').html());
                var item = $('.forum-items').find('.forum-item').first();
                item.find('h3 a').text(topic_title);
                item.find('.post-meta .created').text(formatDate(new Date()));
            });

        });
    });
});