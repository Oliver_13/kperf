$(function() {

    $('#topic_response').click(function() {
        var content = $(this).prev('.content.text'), container = $(this).closest('.form');

        if ((container.find('.messages').length > 0) || (content.text().length < 3)) {
            return false;
        }


        $.when(ajax('ajax_object=ForumMessage&ajax_action=create_message&topic_message='+(content.text()), container))
        .then(function() {
            $('.forum-items').append($('template').html());
            var item = $('.forum-items').find('.forum-item').last();
            item.find('.created').text(formatDate(new Date()));
            item.find('.apercu').text(content.text());
            setTimeout(function() {
                content.text('');
                container.find('.messages').remove();
                $('.alert-warning').remove();
            }, 3000);
        });

    });
});