$(function() {
    // Navigation
    var page_id = $('#page_id').val(),
    panel_nav = $('#sidebar'),
    contentType = $('#contentType').val();

    if (contentType == undefined) {
        contentType = 'editable';
    }


    target = panel_nav.find('.'+contentType+'[data-id="'+page_id+'"]');

    console.log(contentType, target.length);

    if (target.length) {
        if (target.parent().hasClass('submenu')) {
            target.parent().addClass('nav-show').css('display','block');
            target.parent().parent().addClass('open');
        }
        target.addClass('active');
    } else {
        target = panel_nav.find('.listable[data-id="'+page_id+'"]');
        if (target.length) {
            target.addClass('active');
        }
    }
});