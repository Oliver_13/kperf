
function post_deleteAttachedMedia(result)
{
    console.log('Removing '+post_deleteAttachedMedia.name, result.id);
    var element = '.media_item_'+result.id, parent = $(element).closest('.media-gallery');
    $(element).remove();
    pushMediaCounter(parent);
}

function deleteAttachedMedia()
{
    $('a.delete_media').off().on('click', function(e) {
        e.preventDefault();
        console.log(deleteAttachedMedia.name);
        var t = $(this);
        ajax("object_id="+$(this).attr('data-id')+"&ajax_object=MediaManager&ajax_action=remove&callback=post_deleteAttachedMedia&media_type="+$(this).attr('data-media'),  t);
    });
}

function after_upload_distribute_document(data) {
    console.log('after_upload_distribute_document');

    $('.loading-cog-1').remove();

    var uploaded_type = data.input.uploadable_type;
    var dispatcher = $('#wa_uploaded_images .'+uploaded_type+'-bloc');
    var titre = data.input.description == '' ? data.http : data.input.description;
    var media =
    '<div class="line media_item_'+data.uploaded_id+'">'+
    '<a target="_blank" href="'+data.http+'">'+ titre +'</a>'+
    '<a href="#" class="btn btn-danger btn-xs delete_media delete_media" data-media="'+uploaded_type+'" data-id="'+data.uploaded_id+'"><i class="fa fa-trash"></i></a>'+
    '</div>';

    $('.media_type_'+uploaded_type+'.form').find('input, textarea').val('');

    dispatcher.append(media);
    deleteAttachedMedia();
    pushMediaCounter(dispatcher);
}

function pushMediaCounter(container) {
    console.log(container.find('> div').length, container);
    container.find('.counter').text(container.find('> div').length);
}

function after_upload_distribute_image(data) {
    console.log(after_upload_distribute_image.name);

    var dispatcher = $('#wa_uploaded_images .image-bloc');

    var image =
    '<div class="img-holder media_item_'+data.uploaded_id+'">'+
    '<img src="'+data.uploaded_image_thumb+'" alt=""/>'+
    '<div class="text">'+
    '<a href="'+data.uploaded_image+'" target="_blank" data-rel="colorbox"><i class="white fa fa-search bigger-160"></i></a>'+
    '<a href="#" class="delete_media" data-media="image" data-id="'+data.uploaded_id+'"><i class="white fa fa-trash bigger-160"></i></a>'+
    '</div>'+
    '</div>';

    dispatcher.append(image);
    $('.ace-thumbnails [data-rel="colorbox"]').off().colorbox(colorbox_params());
    deleteAttachedMedia();
    pushMediaCounter(dispatcher);

    //var max_photos = parseInt($('#max_photos').val());
    //console.log(max_photos >= $('#wa_uploaded_images img').length, max_photos, $('#wa_uploaded_images img').length);
    /* if (max_photos <= $('#wa_uploaded_images img').length) {
       $("#sup_images_link").removeClass('hidden');
   } */
}

deleteAttachedMedia();
