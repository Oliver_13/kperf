
    function colorbox_params () {
        return {
            reposition:true,
            scalePhotos:true,
            scrolling:false,
            previous:'<i class="fa fa-arrow-left"></i>',
            next:'<i class="fa fa-arrow-right"></i>',
            close:'&times;',
            current:'{current} of {total}',
            maxWidth:'100%',
            maxHeight:'100%',
            onOpen:function(){
                document.body.style.overflow = 'hidden';
            },
            onClosed:function(){
                document.body.style.overflow = 'auto';
            },
            onComplete:function(){
                $.colorbox.resize();
            }
        };
    }

$(function() {



    $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params());
    $("#cboxLoadingGraphic").append("<i class='fa fa-spinner orange'></i>");//let's add a custom loading icon


    $('.input-file').ace_file_input({
        no_file:'Choisissez un fichier ...',
        btn_choose:'Choisissez',
        btn_change:'Modifier',
        droppable:false,
        onchange:null,
        thumbnail:false, //| true | large
        whitelist:'gif|png|jpg|jpeg',
        blacklist:'exe|php'
    });


    function showErrorAlert (reason, detail) {
        var msg='';
        if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
            '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
    }
});