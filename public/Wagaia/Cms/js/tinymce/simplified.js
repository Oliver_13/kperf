var intro_settings = {
        selector: ".simplified",
        theme: "modern",
        width: '100%',
        menubar : false,
        entity_encoding : "raw",
        plugins: [
        "advlist autolink autosave link lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality emoticons template textcolor paste"
        ],
        toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | cut copy paste | bullist numlist | outdent indent blockquote | undo redo | link unlink",
        image_advtab: true ,
        language: "fr_FR",
        language_url :"Wagaia/Cms/js/tinymce/langs/fr_FR.js"

    }