$(function() {

    function replicator_delete() {
        //console.log('replicator_delete is declared');
        $('button.delete_replica').off().click(function(e) {
            e.preventDefault();
            var target = $(this).attr('data-target');
            if (target == 'replicate') {
                $(this).closest('.'+target).remove();
            } else {
                ($(this).closest('.replicate').find('.'+target).length > 2) ? $(this).closest('.'+target).remove() : $(this).closest('.'+target).find('input,textarea').val('');
            }
        });
    }

    function replicator(element) {

        $('button.'+element).off().click(function(e) {

            e.preventDefault();
            var fromTemplate = $('template.'+element).length;
            if (fromTemplate) {
                var replicated = $('<div class="replicate row">'+$('template.'+element).html()+'</div>');
            } else {
                var replicate = $(this).prev('div.'+element);
                var replicated = replicate.clone();
            }

            var replicate_id = guid();
            replicated.find('.replicate_id').val(replicate_id);
            replicated.find('.replicate_fields:not(:first, button)').remove();
            var hasParentContainer = ($(this).attr('data-replica') == 'replicate_fields'), suffixe = hasParentContainer? '[]' : '';

            if (hasParentContainer) {
                var replicate_id = $(this).closest('div.replicate.row').find('.replicate_id').val();
            }

            replicated.find('input:not(.replicate_id)').each(function() {
                $(this).attr('name', 'replica_content['+($(this).closest('.form').find('.content_key').text())+']['+replicate_id+']'+suffixe);
            });
            replicated.find('input:not(.replicate_id, :radio)').each(function() {
                $(this).attr('name', 'replica_content['+($(this).closest('.form').find('.content_key').text())+']['+replicate_id+']'+suffixe).val('');
            });

            if (!fromTemplate) {
                $(replicated).insertAfter(replicate);
            } else {
                replicated.find('.replicate_fields input').each(function() {
                    $(this).attr('name', $(this).attr('name')+'[]');
                });
                $('.replica_container').append(replicated);
            }

            $('div.replicate').last().find('.replicatable').each(function() {
                replicator($(this).attr('data-replica'));
            });
            replicator_delete();

        });
    }

    replicator('replicate');
    replicator('replicate_fields');
    replicator_delete();

});