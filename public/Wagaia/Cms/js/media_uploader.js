$(function() {

    function getAcceptableTypes() {
        return eval($('input[name="acceptable_types"]').val());
    }

    // Media radio choices
    if ($("input.media-selectable").length) {
        if ($("input.media-selectable :checked").length < 1) {
            $('.fileupload-container').hide();
        }
        $("input.media-selectable").click(function() {
            var media_channel = $(this).attr('data-type');
            var media_type = $(this).attr('data-media');
            console.log(media_type);
            $('.fileupload-container input[name="uploadable_type"]').val(media_type);
            if (media_channel == 'fileupload') {
                $(this).closest('.form').find('[class^="media_type_"]').addClass('hidden');
                $('.fileupload-container').show();
                $('.fileupload-container input[name="acceptable_types"]').val($(this).attr('data-acceptable'));
                if ($(this).attr('data-config') != undefined) {
                    $('.fileupload-container input[name="uploadable_config"]').val($(this).attr('data-config'));
                } else {
                    $('.fileupload-container input[name="uploadable_config"]').val('');
                }
            } else {
                $('.fileupload-container').hide();
                $('.fileupload-container input[name="uploadable_config"]').val('');
                $(this).closest('.form').find('.media_type_' + media_type).removeClass('hidden');
            }
        });
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ajax_request_url(),
        type: 'POST',
        dataType:'json',
    });

    console.log(getAcceptableTypes());

    // CONVENTIONAL TEXT UPLOADS

    $('button.upload_media_type').click(function(e) {
        e.preventDefault();
        console.log('Uploading textual type');
        var btn = $(this);
        btn.find('.loading-cog-1').remove();
        btn.append('<img class="loading-cog-1" src="Wagaia/Cms/css/cogs.svg" alt="" />');

        var container = $(this).closest('.form');
        var formData = [];

        var content = container.find("input[name='content']");
        var pattern = new RegExp(content.val());
        if (!isUrlValid(content.val())) {
            content.addClass('required');
            setTimeout(function() {
                btn.find('.loading-cog-1').remove();
            },1000);
            return false;
        } else {
            content.removeClass('required');
        }
        formData.push(
            {name: "page_id", value: $('#page_id').val()},
            {name: "uploadable_type", value: $('input[name="uploadable_type"]').val()},
            {name: "ajax_object", value: 'MediaUploader'},
            {name: "ajax_action", value: 'upload'},
            {name: "content", value: container.find('input').val()},
            {name: "description", value: container.find('textarea').val()},
            {name: "callback", value: 'after_upload_distribute_document'}
            );
        ajax(formData,container);
    });

    // THE FILEUPLOAD MANAGER

    $('#fileupload').fileupload({
        url: ajax_request_url(),
        type: 'POST',
        dataType:'json',
        context: $('#fileupload')[0],
        done: function (e, data)
        {
            $('.progress').hide();

            // Auto-detect Callback
            var fn = window['after_upload_distribute_'+data.result.input.uploadable_type];
            if(typeof fn === 'function') {
                fn(data.result)
            }

            $('tbody.files').delay(3000).fadeOut(function(){
                $('tbody.files').html('').show();
            });
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            var result = ' Error status : '+ xhr.status+ ", Thrown Error : "+ thrownError +", Error : "+ xhr.responseText;
            $('#AjaxSqlResponse').html('<div class="alert alert-danger" style="margin-top:10px;padding:8px 15px">'+result+'</div>')
        },
        always: function(e,data)
        {
            $('.progress').hide();
        },
        start: function(e,data)
        {
            $('.progress').show();
        },
            //acceptFileTypes: getAcceptableTypes(),
            maxFileSize: 16000000,
        //maxNumberOfFiles: 1,
        autoUpload:false,
        //messages : { maxNumberOfFiles: $('#imp .messages .maxNumberOfFiles').text() }
    });

    $('#fileupload').bind('fileuploadsubmit', function (e, data)
    {
        $('#imp div.errors').remove();
        /*
        if ($("#wa_uploaded_images img").length >= parseInt($('#max_photos').val())) {
            $('#imp').append('<div class="errors">'+$('#imp .messages .max_photos_m').text()+'</div>');
            return false;
        }
        */

        data.formData = [];
        data.formData.push(
            {name: "page_id", value: $('#page_id').val()},
            {name: "uploadable_type", value: $('input[name="uploadable_type"]').val()},
            {name: "uploadable_config", value: $('input[name="uploadable_config"]').val()},
            {name: "ajax_object", value: 'MediaUploader'},
            {name: "ajax_action", value: 'upload'},
            {name: "description", value: data.context.find('textarea[name="file_description"]').val()},
            );
    });

    $('#fileupload').bind('fileuploadadd', function (e, data) {
        if (!$('.fileupload-container').is(':visible')) {
            data.abort();
        }


        var uploadErrors = [];
        var acceptFileTypes = getAcceptableTypes();
        console.log(acceptFileTypes.test(data.files[0]['type']));

        if(data.files[0]['type'].length && !acceptFileTypes.test(data.files[0]['type'])) {
            uploadErrors.push('Not an accepted file type');
            return false;
        }

    });
    /*
    $('#fileupload').bind('fileuploadadd', function (e, data) {

        var reader = new FileReader();

        data.files.forEach(function (item, index) {
            reader.readAsDataURL(item);
            reader.data = data;
            reader.file = item;
            reader.onload = function (_file) {

                var image = new Image();

                image.src = _file.target.result;
                image.file = this.file;
                image.data = this.data;
                image.onload = function () {
                    var w = this.width,
                    h = this.height,
                    n = this.file.name;

                    if (w < 800 || h < 600) {
                        data.files.error = true;
                        item.error = $('#imp .messages .dimensions').text();
                        var error = item.error;
                        $(data.context[index]).find(".error").text(error);
                    }

                };
            };
        });
    });
    */
});