$(document).ready(function() {
    var tableSorting = (typeof dataTable_sorting !== 'undefined') ? dataTable_sorting : [0, 'desc'];
    var handleDataTableButtons = function() {
        if ($(".dataTable").length) {
            $(".dataTable").DataTable({
                "lengthChange": false,
                "pageLength": 20,
                dom: "Bfrtip",
                language: {
                    "sSearch": "Recherche : ",
                    "sProcessing":   "Traitements des résultats...",
                    "sLengthMenu":   "Affichage de _MENU_ résultats",
                    "sZeroRecords":  "Aucun résultat",
                    "sInfo":         "Affichage _START_ à _END_ de _TOTAL_ résultats",
                    "sInfoEmpty":    "Aucun résultat",
                    "sInfoFiltered": "( _MAX_ résultats)",
                    "paginate": {
                      "first":      "Premier",
                      "last":       "Dernier",
                      "next":       "Suivant",
                      "previous":   "Précedent"
                  },
              },
              aaSorting : [tableSorting],
              buttons: [
              {
                extend: "copy",
                className: "btn-sm"
            },
            {
                extend: "csv",
                className: "btn-sm"
            },
            {
                extend: "excel",
                className: "btn-sm"
            },
            {
                extend: "pdfHtml5",
                className: "btn-sm"
            },
            {
                extend: "print",
                className: "btn-sm"
            },
            ],
            responsive: true
        });
        }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
      }
  };
}();

TableManageButtons.init();
});

$.fn.dataTable.ext.errMode = 'none';