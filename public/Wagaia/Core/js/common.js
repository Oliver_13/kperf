$.fn.hasParent = function (e) {
    return !!$(this).parents(e).length
}
var token = function() {
  return $('meta[name="csrf-token"]').attr('content');
},
base_url = function(param) {
  param = param ? param : '';
    //console.log($('base').attr('href') + param);
    return $('base').attr('href') + param;
},
ajax_request_url = function() {
    return base_url()+'panel/cms/ajax';
},
spinner = '<i class="kvasir spinner fa fa-cog fa-spin fa-fw"></i>',
timerDefault = function() {
    return 500;
},
setDelay = (function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})(),
timer = timerDefault(),
spinout = function() {
    setTimeout(function() {
        $('.spinner').fadeOut(function() {
            $(this).remove();
        });
    }, timer+timerDefault());
},
notificationQueue = function(messages) {
    return messages.find(' > div').length;
},
notificator_dismiss = function(messages) {
    var delay_sum = timer*(notificationQueue(messages)+1), delays = delay_sum > 3000 ? delay_sum : 3000;
    setTimeout(function() {
      timer = timerDefault();
      messages.find('> div').each(function(index) {
        $(this).delay(timer*(index)).fadeOut(timer*(index+1), function() {
          $(this).remove();
      })
    });
  }, delays);
},
notificator = function(status, data, messages) {
    console.log(messages, status);
    messages.html('');
    if (status == 200) {
        $(data).each(function(index,message) {
            timer = timerDefault()*(notificationQueue(messages)+1);
            $.each(message, function(key,value) {
                alertDispatcher(value, messages, key);
            });
        });
    } else if(status == 422) { // Laravel JSON Validator Messages
        if (data.responseJSON.hasOwnProperty('errors')) {
            $.each( data.responseJSON.errors , function( key, value ) {
                alertDispatcher(value[0], messages, 'danger');
            });
        }
    } else if (status == 404 || status == 500 || status == 401) {
        alertDispatcher($('#js_'+status).text(), messages, 'danger');
    }
    else {
        if (data.hasOwnProperty('responseJSON')) {
            if (data.responseJSON.messages.length > 0) {
                $.each( data.responseJSON.messages , function( key, value ) {
                    $.each(value, function(message_key, message_text) {
                        alertDispatcher(message_text, messages, message_key);
                    });
                });
            }
        }
    }
    dismissable();
},
alertDispatcher = function(message, messages, messageType) {
    var alert = '<div style="display:none;" class="alert alert-dismissible alert-'+messageType+'">';
    alert = alert.concat('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>');
    alert = alert.concat(message+'</div>');
    messages.append(alert);
    messages.find('div:last').fadeIn(timer);
},
ajax = function(formData, selector) {
    console.log(selector, $(this));

    // Le conteneur pour les réponses de la requête
    if (typeof selector == 'undefined') {
        var selector = $(this).closest('.form');
    }

    var ajax_url = ajax_request_url(),
    formElement = selector.closest('.form');console.log(formElement);

    if (selector[0].hasAttribute('tagName') && (selector[0].tagName == 'FORM' && selector[0].action !='')) {
        ajax_url = selector[0].action; console.log('case 1');
    } else if (selector[0].hasAttribute('data-url')) {
        ajax_url = base_url()+selector.attr('data-url'); console.log('case 2');
    } else if (formElement.length) {
        if (formElement[0].hasAttribute('data-url')) {
            ajax_url = base_url()+formElement.attr('data-url'); console.log('case 3');
        } else if (formElement[0].hasAttribute('action') && formElement[0].action !='') {
            ajax_url = formElement[0].action;  console.log('case 4');
        }
    }
    console.log(ajax_url);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ajax_url,
        type: 'POST',
        dataType:'json'
    });
    console.log('Ajax started on '+ ajax_url);
    // return false;

    if (selector.find('.messages').length < 1) {
        console.log('I have appended messages');
        selector.append('<div class="messages"></div>');
    }
    var messages = selector.find('.messages');
    // console.log(selector); console.log(messages);
    $.ajax({
        data: formData,
        done: function() {
            messages.html('');
        },
        success: function(result) {
            console.log(result);
            // Traitements des messages définis par l'utilisateur
            if (result.hasOwnProperty('messages')) {
                notificator(200, result.messages, messages);
            }
            if (result.hasOwnProperty('redirect_to')) {
                setTimeout(function() {
                    window.location = result.redirect_to;
                }, 991500);
            }
            // Si un callback est spécifié, il est exécuté
            var executable_callback = null;
            console.log(result);
            console.log(result.input.callback);
            console.log(result.hasOwnProperty('input.callback'));
            console.log(result.hasOwnProperty('callback'));
            if (result.hasOwnProperty('input.callback')) {
                executable_callback = result.input.callback;
                console.log(result.input.callback, 'input.callback');
            }
            if (result.hasOwnProperty('callback')) {
                executable_callback = result.callback;
                 console.log(result.callback, 'result.callback');
            }
            console.log('executable_callback', executable_callback, result);
            if (executable_callback !== null) {
                var is_callback_object = executable_callback.split('.');
                callback = (is_callback_object.length > 1) ? is_callback_object[0] : executable_callback;
                var fn = window[callback];
                if(typeof fn === 'object') {
                    if (is_callback_object.length < 3) { // var.method
                        window[callback][is_callback_object[1]](result);
                    } else { // var.var.method
                        window[callback][is_callback_object[1]][is_callback_object[2]](result);
                    }
                }
                if(typeof fn === 'function') {
                    fn(result)
                }
            }
        },
        error: function (xhr) {
            console.log(xhr);
            notificator(xhr.status, xhr, messages);
        }
    }).always(function() {
      spinout();
  });
},
slugify = function(text){
    return text.toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\u0100-\uFFFF\w\-]/g,'-').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '');
},
guid = function() {
    return Math.random().toString(36).substr(2, 9);
},
access_key = function() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
},
isUrlValid = function (userInput) {
    var regexQuery = "^(https://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";
    var url = new RegExp(regexQuery,"i");
    return url.test(userInput);
},
dismissable = function() {
    $('.alert-dismissible button').off().on('click', function() {
        $(this).parent().remove();
    });
},
ajaxable = function() {
    $('.ajaxable').off().on('click', function(e) {
        $('div.messages').remove();
        e.preventDefault();
        $('#uncachtableType').remove();
        $(this).append(spinner)
        $(this).find('i.spinner').fadeIn();
        if (typeof tinyMCE != 'undefined') {
            if (tinyMCE.editors.length > 0) {
                tinyMCE.triggerSave();
            }
        }
        var form = $(this).closest('.form'),
        ajaxableType = $(this).attr('type');
        if ( ajaxableType == 'button' || ajaxableType == 'submit' ) {
            form.append('<input id="uncachtableType" type="hidden" name="'+$(this).attr('name')+'" value="'+$(this).val()+'"/>');
        }
        ajax(form.find('input, select, textarea').serialize(), form);
    });
};

$(function() {
  ajaxable();
});