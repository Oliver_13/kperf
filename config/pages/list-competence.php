<?php

return [
	'no_links',
	'no_text',
	'no_meta',
	'has' => [
        'competence' => [
            'label'=>'Images'
        ]
    ],
];