<?php

return [
	'intro',
	'no_text',
	'subtitle',
	'no_meta',
    'images' => [
        'image_main'=> [
            'label'=>'Image',
            'size' => ['w'=>1920, 'h'=> 560],
            'jcrop'
        ]
    ],
];