<?php

return [
	'intro',
	'no_text',
	'no_meta',
    'images' => [
        'image_etape'=> [
            'label'=>'Image',
            'size' => ['w'=>800, 'h'=> 660],
            'jcrop'
        ]
    ],
];