<?php

return [
	'intro',
	'no_text',
	'no_meta',
    'images' => [
        'image_etape'=> [
            'label'=>'Image',
            'size' => ['w'=>720, 'h'=> 480],
            'jcrop'
        ]
    ],
];