<?php

return [
	'no_text',
    'no_meta',
    'images' => [
        'image_main'=> [
            'label'=>'Image',
            'size' => ['w'=>150, 'h'=> 150],
            'jcrop'
        ]
    ],
];