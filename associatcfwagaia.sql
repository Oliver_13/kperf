-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : associatcfwagaia.mysql.db
-- Généré le :  lun. 17 sep. 2018 à 11:22
-- Version du serveur :  5.6.39-log
-- Version de PHP :  7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `associatcfwagaia`
--

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_categories`
--

CREATE TABLE `wagaia_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lg` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'fr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_config`
--

CREATE TABLE `wagaia_config` (
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_custom_content`
--

CREATE TABLE `wagaia_custom_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `pages_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `lg` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT 'fr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_forum`
--

CREATE TABLE `wagaia_forum` (
  `id` int(10) UNSIGNED NOT NULL,
  `pages_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `topic_title` text COLLATE utf8_unicode_ci,
  `topic_message` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `visibility` int(1) DEFAULT '1',
  `approved` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nav_url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_forum_messages`
--

CREATE TABLE `wagaia_forum_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `forum_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_geocode`
--

CREATE TABLE `wagaia_geocode` (
  `id` int(10) UNSIGNED NOT NULL,
  `street_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `locality` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `administrative_area_level_1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `lon` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_global`
--

CREATE TABLE `wagaia_global` (
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `lg` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_mails`
--

CREATE TABLE `wagaia_mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_message` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `consent` timestamp NULL DEFAULT NULL,
  `key` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_media_content`
--

CREATE TABLE `wagaia_media_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `pages_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `varname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_media_content_description`
--

CREATE TABLE `wagaia_media_content_description` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_content_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `lg` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'fr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_nav`
--

CREATE TABLE `wagaia_nav` (
  `id` int(10) UNSIGNED NOT NULL,
  `pages_id` int(10) UNSIGNED DEFAULT NULL,
  `is_primary` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pull_children` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_pages`
--

CREATE TABLE `wagaia_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taxonomy` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_key` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config` blob,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_pages_categories`
--

CREATE TABLE `wagaia_pages_categories` (
  `page_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_pages_data`
--

CREATE TABLE `wagaia_pages_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `pages_id` int(10) UNSIGNED NOT NULL,
  `titre` text COLLATE utf8_unicode_ci,
  `sous_titre` text COLLATE utf8_unicode_ci,
  `intro` text COLLATE utf8_unicode_ci,
  `texte` text COLLATE utf8_unicode_ci,
  `texte_level_2` text COLLATE utf8_unicode_ci,
  `texte_level_3` text COLLATE utf8_unicode_ci,
  `texte_level_4` text COLLATE utf8_unicode_ci,
  `meta_titre` text COLLATE utf8_unicode_ci,
  `meta_key` text COLLATE utf8_unicode_ci,
  `meta_desc` text COLLATE utf8_unicode_ci,
  `nav_title` text COLLATE utf8_unicode_ci,
  `nav_url` text COLLATE utf8_unicode_ci,
  `lg` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT 'fr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wagaia_social`
--

CREATE TABLE `wagaia_social` (
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `lg` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fr'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `wagaia_categories`
--
ALTER TABLE `wagaia_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `wagaia_custom_content`
--
ALTER TABLE `wagaia_custom_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_wagaia_simple_custom_content_wagaia_pages` (`pages_id`);

--
-- Index pour la table `wagaia_forum`
--
ALTER TABLE `wagaia_forum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_id` (`pages_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `wagaia_forum_messages`
--
ALTER TABLE `wagaia_forum_messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `wagaia_geocode`
--
ALTER TABLE `wagaia_geocode`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `wagaia_mails`
--
ALTER TABLE `wagaia_mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `key` (`key`);

--
-- Index pour la table `wagaia_media_content`
--
ALTER TABLE `wagaia_media_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_wagaia_media_content_wagaia_pages` (`pages_id`);

--
-- Index pour la table `wagaia_media_content_description`
--
ALTER TABLE `wagaia_media_content_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_wagaia_media_content_description_wagaia_media_content` (`media_content_id`);

--
-- Index pour la table `wagaia_nav`
--
ALTER TABLE `wagaia_nav`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_id` (`pages_id`);

--
-- Index pour la table `wagaia_pages`
--
ALTER TABLE `wagaia_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Index pour la table `wagaia_pages_categories`
--
ALTER TABLE `wagaia_pages_categories`
  ADD KEY `category_id` (`category_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Index pour la table `wagaia_pages_data`
--
ALTER TABLE `wagaia_pages_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pages` (`pages_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `wagaia_categories`
--
ALTER TABLE `wagaia_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `wagaia_custom_content`
--
ALTER TABLE `wagaia_custom_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15230;
--
-- AUTO_INCREMENT pour la table `wagaia_forum`
--
ALTER TABLE `wagaia_forum`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `wagaia_forum_messages`
--
ALTER TABLE `wagaia_forum_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `wagaia_geocode`
--
ALTER TABLE `wagaia_geocode`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `wagaia_mails`
--
ALTER TABLE `wagaia_mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `wagaia_media_content`
--
ALTER TABLE `wagaia_media_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT pour la table `wagaia_media_content_description`
--
ALTER TABLE `wagaia_media_content_description`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT pour la table `wagaia_nav`
--
ALTER TABLE `wagaia_nav`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `wagaia_pages`
--
ALTER TABLE `wagaia_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT pour la table `wagaia_pages_data`
--
ALTER TABLE `wagaia_pages_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `wagaia_custom_content`
--
ALTER TABLE `wagaia_custom_content`
  ADD CONSTRAINT `FK_wagaia_simple_custom_content_wagaia_pages` FOREIGN KEY (`pages_id`) REFERENCES `wagaia_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wagaia_forum`
--
ALTER TABLE `wagaia_forum`
  ADD CONSTRAINT `FK_wagaia_forum_wagaia_pages` FOREIGN KEY (`pages_id`) REFERENCES `wagaia_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wagaia_media_content`
--
ALTER TABLE `wagaia_media_content`
  ADD CONSTRAINT `FK_wagaia_media_content_wagaia_pages` FOREIGN KEY (`pages_id`) REFERENCES `wagaia_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wagaia_media_content_description`
--
ALTER TABLE `wagaia_media_content_description`
  ADD CONSTRAINT `FK_wagaia_media_content_description_wagaia_media_content` FOREIGN KEY (`media_content_id`) REFERENCES `wagaia_media_content` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wagaia_nav`
--
ALTER TABLE `wagaia_nav`
  ADD CONSTRAINT `FK_wagaia_nav_wagaia_pages` FOREIGN KEY (`pages_id`) REFERENCES `wagaia_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wagaia_pages_categories`
--
ALTER TABLE `wagaia_pages_categories`
  ADD CONSTRAINT `FK_wagaia_pages_categories_wagaia_categories` FOREIGN KEY (`category_id`) REFERENCES `wagaia_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_wagaia_pages_categories_wagaia_pages` FOREIGN KEY (`page_id`) REFERENCES `wagaia_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wagaia_pages_data`
--
ALTER TABLE `wagaia_pages_data`
  ADD CONSTRAINT `FK1_xz` FOREIGN KEY (`pages_id`) REFERENCES `wagaia_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
