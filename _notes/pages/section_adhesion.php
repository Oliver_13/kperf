<?php

return [
    'no_meta',
    'no_title',
    'custom_content' => [
        include(app_path('Airbus/Config/adhesions_renouvellement.php')),
        include(app_path('Airbus/Config/tarifs_adhesions.php'))
    ]
];