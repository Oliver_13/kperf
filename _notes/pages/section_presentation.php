<?php

return [
    'no_meta',
    'no_title',
    'images' =>  [
        'image_main' => [
            'label'=>'Image principale',
            'size' => ['w'=>870, 'h'=> null],
        ]
    ],
    'custom_content' => [
        [
            'multi_lang' => false,
            'label'=>'Widgets',
            'cols' => 'auto',
            'fields' => [
                'meteo' => [
                    'type'=>'textarea',
                    'label'=>'Méteo',
                ],
            ],
        ]
    ],
    'has'=> [
        'subpage' => [
            'label'=>'Pages complémentaires',
            'url_prefix'=> 'contenu',
            'taxonomy'=>'subpages'
        ]
    ]
];