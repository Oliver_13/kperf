<?php

return [
    'images' => [
        'image_main'=> [
            'label'=>'Image',
            'size' => ['w'=>870, 'h'=> 580],
            'jcrop'
        ]
    ],
    'intro',
    'custom_content' => [
        [
            'multi_lang' => false,
            'label'=>'Catégorie',
            'fields' => [
                'category' => [
                    'type'=>'select',
                    'label'=>'Catégorie',
                    'query'=> [
                        'method'=>'categories',
                        'arguments'=> ['type'=>'category', 'taxonomy'=>'evenements']
                    ]
                ],
            ],
        ],
        [
            'multi_lang' => false,
            'label'=>'Dates',
            'fields' => [
                'event_start' => [
                    'type'=>'datepicker',
                    'label'=>'Début',
                    'config'=> [
                        'enableTime' => 'true',
                        'dateFormat'=>"d/m/H H:i",
                    ]
                ],
                'event_end' =>  [
                    'type'=>'datepicker',
                    'label'=>'Fin',
                    'config'=> [
                        'enableTime' => 'true',
                        'dateFormat'=>"d/m/H H:i",
                    ]
                ],
            ],
        ],
        [
            'multi_lang' => false,
            'label'=>"Lieu de l'evènement",
            'fields' => [
                'event_location' => [
                    'type'=>'text'
                ],
            ],
        ],
        [
            'multi_lang' => false,
            'label'=>"Inscription",
            'fields' => [
                'event_subscription'=> [
                    'type'=>'radio',
                    'label'=>'Inscription préalable',
                    'options'=> [
                        'mandatory'=> [
                            'label'=>'Obligatoire',
                        ],
                        'optional'=> [
                            'label'=>'Non-obligatoire',
                        ]
                    ]
                ],
                'event_max_participants'=> [
                    'type'=>'number',
                    'label'=>"Nombre max de particpants",
                ],
                'event_show_participants'=> [
                    'type'=>'radio',
                    'label'=>'Afficher les inscrits',
                    'options'=> [
                        1 => [
                            'label'=>'Afficher',
                        ],
                        0 => [
                            'label'=>'Ne pas afficher',
                        ]
                    ]
                ],
            ],
        ],
        [
            'multi_lang' => false,
            'label'=>"Prix par participant",
            'fields' => [
                'event_price'=> [
                    'type'=>'text'
                ],
            ],
        ],
    ]
];