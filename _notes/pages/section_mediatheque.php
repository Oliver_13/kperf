<?php

return [
    'no_meta',
    'no_title',
    'has'=> [
        'category' => [
            'label'=>'Catégories',
            'url_prefix'=> 'categories',
            'taxonomy'=>'media'
        ],
        'media' => [
            'label'=>'Medias',
            'url_prefix'=>'media',
        ]
    ]
];