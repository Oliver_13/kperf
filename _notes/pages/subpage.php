<?php

return [
    'images' =>  [
        'image_main' => [
            'label'=>'Image principale',
            'size' => ['w'=>870, 'h'=> null],
        ]
    ],
    'has'=> [
        'subpage_child' => [
            'label'=>'Sous-pages',
            'url_prefix'=> 'sous-pages',
            'taxonomy'=>'sous-page'
        ]
    ]
];