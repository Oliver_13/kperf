<?php

return [
    'no_text',
    'archive',
    'images' =>  [
        'image_logo' => [
            'label'=>'Logo',
            'size' => ['w'=>400, 'h'=> null],
        ],
        'image_bandeau' => [
            'label'=>'Image bandeau',
            'size' => ['w'=>1920, 'h'=> null],
        ]
    ],
    'subpages' => [
        'presentation' => 'Présentation',
        'actus'=>'Actualités',
        'evenements'=>'Évènements',
        'forum'=> 'Forum',
        'mediatheque'=>'Médiatheque',
        'boutique'=>'Boutique',
        'contact'=>'Contact',
        'adhesion' => 'Adhésion',
        'boutique'=>'Boutique'
    ],
    'custom_content' => [
        [
            'multi_lang' => false,
            'label'=>'Admin section',
            'cols' => 'auto',
            'fields' => [
                'admin_prenom' => [
                    'type'=>'text',
                    'label'=>'Prénom',
                ],
                'admin_nom' =>  [
                    'type'=>'text',
                    'label'=>'Nom',
                ],
                'email' =>  [
                    'type'=>'email',
                    'label'=>'Adresse e-mail',
                ]
            ],
        ],
        [
            'multi_lang' => false,
            'label'=>'Informations bancaires',
            'cols' => 'auto',
            'fields' => [
                /*
                'site_id' => [
                    'type'=>'text',
                    'label'=>'Identifiant Section',
                ],
                'boutique_id' =>  [
                    'type'=>'text',
                    'label'=>'Boutique ID',
                ],
                */
                'iban' =>  [
                    'type'=>'textarea',
                    'label'=>'IBAN',
                ]
            ],
        ]
    ]
];