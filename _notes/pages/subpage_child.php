<?php

return [
    'images' =>  [
        'image_main' => [
            'label'=>'Image principale',
            'size' => ['w'=>870, 'h'=> null],
        ]
    ],
];