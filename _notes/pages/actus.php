<?php

return [
    'images' => [
        'image_main'=> [
            'label'=>'Image',
            'size' => ['w'=>870, 'h'=> 580],
            'jcrop'
        ]
    ],
    'intro',
    'custom_content' => [
        [
            'multi_lang' => false,
            'label'=>'Catégorie',
            'fields' => [
                'category' => [
                    'type'=>'select',
                    'label'=>'Catégorie',
                    'query'=> [
                        'method'=>'categories',
                        'arguments'=> ['type'=>'category', 'taxonomy'=>'actus','parent'=>'']
                    ]
                ],
            ],
        ]
    ]
];