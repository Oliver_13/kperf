<?php

return [
    'no_meta',
    'no_title',
    'has'=> [
        'evenements' => [
            'label'=>'Evènements',
            'url_prefix'=> 'evenements'
        ]
    ]
];