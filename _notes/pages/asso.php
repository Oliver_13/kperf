<?php

return [
    //'archive',
    'url_prefix'=>'associations',
    'has' => [
        'section' => [
            'label'=>'Sections',
            'archive',
            'url_prefix'=> 'sections',
            'access_key'=>10,
        ]
    ],
    'custom_content' => [
        include(app_path('Airbus/Config/adhesions_renouvellement.php')),
        include(app_path('Airbus/Config/tarifs_adhesions.php'))
    ]
];