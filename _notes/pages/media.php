<?php

return [
    'no_meta',
    'no_text',
    'intro',
    'custom_content' => [
        [
            'multi_lang' => false,
            'label'=>'Catégorie',
            'fields' => [
                'category' => [
                    'type'=>'select',
                    'label'=>'Catégorie',
                    'query'=> [
                        'method'=>'categories',
                        'arguments'=> ['type'=>'category', 'taxonomy'=>'media','parent'=>'']
                    ]
                ],
            ],
        ],
        [
            'multi_lang' => false,
            'label'=>'Type de media',
            'fields' => [
                'media' => [
                    'type'=>'media',
                    'types'=> [
                        'document'=> [
                            'label'=>'Document',
                            'acceptable'=>'/(\.|\/)(pdf)$/i',
                            'type'=>'fileupload'
                        ],
                        'video'=> [
                            'label'=>'Vidéo',
                            'placeholder'=>'Lien de la vidéo',
                            'info'=> "Copiez/collez simplement le lien d'une video disponible sur Youtube, Vimeo, DailyMotion, Vine ou Kickstarter.<br><strong>Exemple :</strong> <em>https://www.youtube.com/watch?v=cq9bBqjknAY</em> ou <em>https://youtu.be/cq9bBqjknAY</em>"
                        ],
                        'image' =>  [
                            'label'=>'Image',
                            'size' => ['w'=>1920, 'h'=> null],
                            'acceptable'=>'/(\.|\/)(gif|jpe?g|png)$/i',
                            'type'=>'fileupload'
                        ],
                    ]
                ],
            ],
        ]
    ]
];