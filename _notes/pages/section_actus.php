<?php

return [
    'no_meta',
    'no_title',
    'has'=> [
        'category' => [
            'label'=>'Catégories',
            'url_prefix'=> 'categories',
            'taxonomy'=>'actus'
        ],
        'actus' => [
            'label'=>'Actualités',
            'url_prefix'=> 'actualites'
        ]
    ]
];