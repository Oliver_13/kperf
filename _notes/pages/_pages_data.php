<?php

return [

    'extend_content'=> [
        [
            'type'=> [
                'section_presentation',
                'section_adhesion',
                'section_actus',
                'section_evenements',
                'section_boutique',
                'section_contact',
                'actus',
                'evenements'

            ],
            'source'=>'text_levels'
        ]

    ]
];