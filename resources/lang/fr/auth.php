<?php

return [

    'failed' => 'Informations invalides',
    'throttle' => 'Vous avez essayé de vous connecter sans succès de nombreuses fois. Veuillez réessayer dans :seconds secondes.',
    'email' => 'Adresse e-mail',
    'password' => 'Mot de passe',
    'loginClient'=>'Accès compte client',
    'keepMe'=>'Garder ma connexion active',
    'loginBtn'=>'Connexion',
    'passwordForgotten'=>'Mot de passe oublié ?',
    'registerClient'=>'Créer un compte',
    'form_account_title'=>'Accès Compte',
    'form_login_title'=>'Accès Panel',

];