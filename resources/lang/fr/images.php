<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Images Language Lines
	|--------------------------------------------------------------------------
	|
	*/


	'home'=>'De présentation',
	'NoResize'=>'sans redimension',
	'OptionnalDesc'=>'Description (optionnelle). Pour suivre un modèle Titre/Texte placez un @ entre les deux',
	'LegendTitle'=>'Légende de la photo',
	'addImages'=>'Ajouter des images...',
	'diapo'=>'Diaporama',
	'inpage'=>'Bloc',
	'images'=>'image|images',
	'gal'=>"Galerie",
	'map'=>'Plan',
	'logo'=>'Logo',
	'sky'=>'Sky',
	'OptionnalLink'=>'Lien (optionnel)',
	'associate'=>'Associer des images',
	'deleteTextSuccess'=>"Le texte de l'image a été supprimé",
	'byDay'=>'Images par étape',
	'day'=>'jour',
	'mediapdf'=>'Documents PDF',



];
