-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : associatcfwagaia.mysql.db
-- Généré le :  lun. 17 sep. 2018 à 11:24
-- Version du serveur :  5.6.39-log
-- Version de PHP :  7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `associatcfwagaia`
--

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Andrian Mihailov', 'amihailov@wagaia.com', '$2y$10$oONeC8qA6XecVAeT76gm2e0KovVUzQ7Rc/lU3Bpmo6DT1rO1mh0VK', 'm0kPGJ7KFLirhKIYrO0yLDxNFRQzEq80KYhmvq4ey2Njt6O581MTgtnngG2w', NULL, NULL),
(2, 'Olivier Brutinel', 'obrutinel@wagaia.com', '$2y$10$5IBWW7Nm65latjam/jxpoejqosYDT2Br2fP93F2CW7utOD6RfoJHu', NULL, NULL, NULL),
(3, 'Jêrome Sabat', 'jsabat@wagaia.com', '$2y$10$GGWlfkbdVqrS4dQLrUmBdeadqHmrc/ypcADAx0Vhq7OhyrxoyMdtC', NULL, NULL, NULL),
(4, 'Christine Alcaraz', 'calcaraz@wagaia.com', '$2y$10$pxYrHguURD1054GFIH5w3.wm6URFBhxkZUVzx1aca5rq.x/ajVxsq', NULL, NULL, NULL),
(5, 'Compte test', 'test@associations-ce-airbushelicopters.com', '$2y$10$THow5ZaiWGUVKCXcrkm0necU3xel04XuSc0/6F2tlInrc.f9ZSGwS', 'WqYO4VxVnvpuLwzt3ypfZDFTujiFSnw8jM8j4m3qK5X1rp3PGVzVrggfJEZR', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
