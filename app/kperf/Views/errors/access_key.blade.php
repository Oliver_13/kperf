@extends('front.layout')
@section('meta_title', '404 Wagaia')

@section('content')

<p style="margin: 100px 50px" class="alert alert-danger">{{ trans('wagaia.http.401') }}</p>

@endsection