{!! section_navbar($data->hasParent) !!}
<li>
    <a href="{!! url('panel/cms/pages/edit/'.$data->hasParent->parent.'/subpage/'.str_replace('section_','', $data->hasParent->type)) !!}">{!! $data->hasParent->titre !!}</a>
</li>
<li>
    <a href="{!! url('panel/cms/pages/list/'.$data->parent.'/'.$data->type) !!}">Liste</a>
</li>
@set('section_parent_config', config('pages.'. $data->hasParent->hasParent->hasParent->type))

@php
global $ariane_url;
$ariane_url = $section_parent_config['url_prefix'].'/'.
$data->hasParent->hasParent->meta->nav_url.'/'.
$section_parent_config['has']['section']['url_prefix'].'/'.
$data->hasParent->hasParent->meta->nav_url .'/'.
str_replace('section_','', $data->hasParent->type).'/'.
$data->id.'-'.$data->meta->nav_url
@endphp