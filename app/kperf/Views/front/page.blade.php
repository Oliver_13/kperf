@extends('front.layout')

@section('content')

    <section class="title_container start-style">
        <div class="page-section-content overflow-hidden">
            <div class="container page-section ">
                <h2 class="uppercase text-start">{!! $data->titre !!}</h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="page-section-content overflow-hidden">
            <div class="container">
                <div class="rows ok-row">
                    <div class="ok-md-12">
                        {!! $data->texte !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

