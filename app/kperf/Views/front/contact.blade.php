@extends('front.layout')

@section('content')

    <section class="title_container start-style">
        <div class="page-section-content overflow-hidden">
            <div class="container page-section">
                <h2 class="uppercase text-start">{!! $data->titre !!}</h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="page-section-content overflow-hidden">
            <div class="container">

                <div class="ok-md-8">

                    @if($data->texte !== '')
                        <div class="dima-container float-start">
                            {!! $data->texte !!}
                        </div>
                        <hr />
                    @endif
                   
                    <div>

                        @if ($errors->any())
                            <div id="contactError" class="dima-alert dima-alert-error">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br />
                                @endforeach
                            </div>
                        @endif

                        @if(session()->has('message'))

                            <div id="contactSuccess" class="dima-alert dima-alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{ session()->get('message') }}
                            </div>

                        @else

                            <form method="POST" action="{!! url()->full() !!}" class="form-small form text-center">
                                @csrf
                                <div class="ok-row">
                                    <div class="post ok-md-4 ok-xsd-6">
                                        <div class="field">
                                            <input type="text" placeholder="Nom*" name="name" value="{!! request()->name !!}">
                                        </div>
                                    </div>
                                    <div class="post ok-md-4 ok-xsd-6  ">
                                        <div class="field">
                                            <input type="email" placeholder="Email*" name="email" value="<?=request()->email?>">
                                        </div>
                                    </div>
                                    <div class="post ok-md-4 ok-xsd-6  ">
                                        <div class="field">
                                            <input type="text" placeholder="Sujet*" name="subject" value="<?=request()->subject?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <textarea class="textarea" placeholder="Message*" name="message"><?=request()->message?></textarea>
                                </div>
                                <input type="submit" value="ENVOYER" class="no-rounded button small fill">
                            </form>

                        @endif

                    </div>

                </div>


                 <div class="ok-md-4">
                    @if($data->intro !== '')
                        {!! $data->intro !!}
                    @endif
                </div>

            </div>
        </div>
    </section>

@stop

