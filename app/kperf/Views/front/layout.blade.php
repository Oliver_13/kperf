<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="fr-FR">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <base href="{!! url('/') !!}/"/>
    <title>{{ ($data->meta_titre ?? null) . ' KPERF' }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="{{ $data->meta_desc ?? null }}">
    <meta name="keywords" content="{{ $data->meta_key ?? null }}">
    <meta name="author" content="Wagaia, agence web sur Marseille">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="stylesheet" data-them="" href="{!! asset($project.'/css/styles.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset($project.'/js/specific/revolution-slider/css/settings.css') !!}" media="screen" />

    <!--[if IE]>
        <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- Googl Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,400,600,700,300" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{!! asset($project.'/images/favicon.png') !!}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{!! asset($project.'/images/favicon.png') !!}" type="image/x-icon" />

    {!! csscrush_inline(public_path($project.'/css/wagaia.css'), ['minify'=>true]) !!}

    @stack('css')

</head>
<body id="@yield('page_id')" class="@yield('page_class') responsive">

    <div class="all_content">
            
        <div class="dima-loading">
            <span class="loading-top"></span>
            <span class="loading-bottom"></span>
            <span class="spin-2"><p>CHARGEMENT...</p>
        </div>

        <header role="banner">
            
            <div class="dima-navbar-wrap dima-navbar-fixed-top-active dima-topbar-active desk-nav">
                <div class="dima-navbar fix-one">
                    <div class="container">
                        
                        <a class="dima-btn-nav" href="#"><i class="fa fa-bars"></i></a>
                        
                        <div class="logo">
                            <h1>
                                <a data-animated-link="fadeOut" href="{!! url('/') !!}" title="PixelDima.com logo">
                                    <span class="vertical-middle"></span>
                                    <img src="{!! asset($project.'/images/logo.png') !!}" alt="PixelDima Logo" title="PixelDima">
                                </a>
                            </h1>
                        </div>
                        
                        <nav role="navigation" class="clearfix">
                            <ul class="dima-nav">
                                @foreach($nav as $page)
                                    <li><a data-animated-link="fadeOut" href="{{ $page['url'] }}">{{ $page['title'] }}</a></li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                   
                </div>
                <div class="clear-nav"></div>
            </div>
            
            <div class="dima-navbar-wrap mobile-nav">
                <div class="dima-navbar fix-one">
                    <div class="container">
                        
                        <a class="dima-btn-nav" href="#"><i class="fa fa-bars"></i></a>
                        
                        <div class="logo">
                            <h1>
                            <a data-animated-link="fadeOut" href="{!! url('/') !!}" title="PixelDima.com logo">
                            <span class="vertical-middle"></span>
                            <img src="{!! asset($project.'/images/logo.png') !!}" alt="" title="PixelDima">
                            </a>
                            </h1>
                        </div>
                        
                        <nav role="navigation" class="clearfix">
                            <ul class="dima-nav">
                                @foreach($nav as $page)
                                    <li><a data-animated-link="fadeOut" href="{{ $page['url'] }}">{{ $page['title'] }}</a></li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            
        </header>

        @yield('content')

        @php
            /*
            $footer = $metaData->where('type','footer')->first();
            $company = $metaData->where('type','company')->first()->simpleCustomContent->pluck('content','field')->toArray();
            */
        @endphp

            <footer role="contentinfo" class="dima-footer">
                <div class="container">
                    <div class="ok-row">
                        
                        <div class="ok-md-4 ok-xsd-12 ok-sd-12">
                            <div class="dima-center-full copyright">
                                <p>© 2018 - KPERF
                                </p>
                            </div>
                        </div>
                        
                        <div class="ok-md-8 ok-xsd-12 ok-sd-12 hidden-xsd">
                            <ul class="dima-center-full dima-menu">
                                @foreach($nav as $page)
                                    <li><a data-animated-link="fadeOut" href="{{ $page['url'] }}">{{ $page['title'] }}</a></li>
                                @endforeach
                                <li>{!! App\kperf\Helpers\Helpers::getLink(93, true) !!}</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </footer>

            <script src="{!! asset($project.'/js/core/jquery-2.1.1.min.js') !!}"></script>
            <!--
                <script src="https://ajax.googleapis.com/ajax/module/jquery/2.1.3/jquery.min.js"></script>
                <script>window.jQuery || document.write('<script src="js/module/jquery-2.1.1.min.js"><\/script>')</script>
            -->
            <script src="{!! asset($project.'/js/core/load.js') !!}"></script>
            <script src="{!! asset($project.'/js/core/jquery.easing.1.3.js') !!}"></script>
            <script src="{!! asset($project.'/js/core/modernizr-2.8.2.min.js') !!}"></script>
            <script src="{!! asset($project.'/js/core/imagesloaded.pkgd.min.js') !!}"></script>
            <script src="{!! asset($project.'/js/core/respond.src.js') !!}"></script>
            <script src="{!! asset($project.'/js/libs.min.js') !!}"></script>

            <script src="{!! asset($project.'/js/specific/mediaelement/mediaelement-and-player.min.js') !!}"></script>
            <script src="{!! asset($project.'/js/specific/video.js') !!}"></script>
            <script src="{!! asset($project.'/js/specific/bigvideo.js') !!}"></script>

            <script src="{!! asset($project.'/js/main.js') !!}"></script>

            <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
            <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
            <script>
            window.addEventListener("load", function(){
            window.cookieconsent.initialise({
              "palette": {
                "popup": {
                  "background": "#eeeeee"
                },
                "button": {
                  "background": "#4f4399"
                }
              },
              "content": {
                "message": "En poursuivant votre navigation sur nos sites, vous acceptez l'installation et l'utilisation de cookies sur votre poste, notamment à des fins promotionnelles et/ou publicitaires,\ndans le respect de notre politique de protection de votre vie privée. Pour plus d'informations, consultez nos",
                "dismiss": "J'ai compris",
                "link": "mentions légales",
                "href": "{!! url('/') !!}/mentions-legales"
              }
            })});
            </script>            
            
        </div>
        
        @stack('js')
    </body>
</html>