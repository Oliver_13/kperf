<!doctype html>
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title></title>
    
    <style type="text/css">
      body {
        width: 100%;
        margin: 0;
        padding: 0;
        -webkit-font-smoothing: antialiased;
      }
    </style>
  </head>
  
  <body style="font-family: Open sans, sans-serif; font-size:13px; color: #20201e; min-height: 200px;" bgcolor="#e6e7e8" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
  
    <table width="100%" height="100%" bgcolor="#e6e7e8" cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td width="100%" align="center" valign="top" bgcolor="#e6e7e8" style="background-color:#e6e7e8; min-height: 200px;">
          <table>
            <tr>
              <td class="table-td-wrap" align="center" width="608">
              
                <table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e6e7e8;" width="600" bgcolor="#e6e7e8" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e6e7e8;" width="600" bgcolor="#e6e7e8" align="left">&nbsp;</td>
                  </tr>
                </table>
                
                <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td>
                  </tr>
                </table>

                <table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-row-td" style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">
                    
                      <table class="table-col" align="left" width="552" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                        <tr>
                          <td class="table-col-td" width="552" style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; font-weight: normal;" valign="top" align="left">  
                          
                            <div style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; text-align: center;">
                              <a href="{!! url()->full() !!}" style="color: #337ab7; text-decoration: none; background-color: transparent;">
                                <img src="{!! asset($project.'/images/logo.png') !!}" alt="" style="margin: 0 auto;border: 0px none #337ab7; vertical-align: middle; display: block; padding-bottom: 0; width: 170px;" hspace="0" vspace="0" border="0" />
                              </a>
                            </div>
                            
                          </td>
                        </tr>
                      </table>
                      
                    </td>
                  </tr>
                </table>
                
                <table class="table-space" height="24" style="height: 24px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-space-td" valign="middle" height="24" style="height: 24px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="center">&nbsp;
                      <table bgcolor="#e8e8e8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td bgcolor="#e8e8e8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                
                <table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-row-td" style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
                      <table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                        <tr>
                          <td class="table-col-td" width="528" style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; font-weight: normal;" valign="top" align="left">
                            Bonjour,<br /><br />
                            Vous avez reçu une nouvelle demande de contact :
                            <ul>
                              <li><strong>Nom</strong> : {{ $data['name'] }}</li>
                              <li><strong>Email</strong> : {{ $data['email'] }}</li>
                              <li><strong>Sujet</strong> : {{ $data['subject'] }}</li>
                              <li><strong>Message</strong> : {{ $data['message'] }}</li>
                            </ul>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                
                <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td>
                  </tr>
                </table>
                
                <table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e6e7e8;" width="600" bgcolor="#e6e7e8" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e6e7e8;" width="600" bgcolor="#e6e7e8" align="left">&nbsp;</td>
                  </tr>
                </table>
                
                <table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-row-td" style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
                      <table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                        <tr>
                          <td class="table-col-td" width="528" style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #20201e; font-size: 13px; font-weight: normal;" valign="top" align="left">
                          
                            <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                              <tr>
                                <td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" align="left">&nbsp;</td>
                              </tr>
                            </table>
                            
                            <div style="font-family: 'Open sans', sans-serif; line-height: 17px; color: #777777; font-size: 14.3px; text-align: center;">&copy; 2018 - Kperf</div>
                            
                            <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                              <tr>
                                <td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" align="left">&nbsp;</td>
                              </tr>
                            </table>
                            
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                
                <table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: #e6e7e8;" width="600" bgcolor="#e6e7e8" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: #e6e7e8;" width="600" bgcolor="#e6e7e8" align="left">&nbsp;</td>
                  </tr>
                </table>
                
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
  </body>
</html>