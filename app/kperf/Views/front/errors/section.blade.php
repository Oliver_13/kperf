@extends('front.layout')

@push('css')
@endpush

@section('page_class','section')

@section('content')

@set('img_bandeau', $data->mediaContent->filter(function($item) { return $item->varname == 'image_bandeau';})->first())
<div class="inner-banner text-center" style="{!! !is_null($img_bandeau) ? 'background: url('.asset('upload/images/'.$data->access_key.'/'.$img_bandeau->content).')' : null  !!}">
    <div class="container">
        <div class="box">
            <h3>Section {{ $data->content->titre }}</h3>
            <div class="pull-right">
                <img src="images/kperf/asah.png" />
            </div><!-- /.pull-right -->
        </div><!-- /.box -->
        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-left">
                    @set('asso_url', config('pages.'.$data->hasParent->type.'.url_prefix').'/'.$data->hasParent->meta->nav_url)
                    <ul class="list-inline link-list">
                        <li><a href="{!! url('/') !!}">Accueil</a></li>
                        <li><a href="{!! url($asso_url) !!}">{!! $data->hasParent->meta->titre !!}</a></li>
                        <li><a href="{!! url($asso_url.'/sections') !!}">Sections</a></li>
                        <li>{{ $data->content->titre }}</li>
                    </ul>
                </div>
            </div><!-- /.container -->
        </div>
    </div><!-- /.container -->
</div>

<section class="service style-2 sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="service-sidebar">
                    @include('front.sections.inc.navbar_vertical')
                    <div class="contact-info2" id="meteo">
                    </div>

                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-12">
                <h2>Une erreur est survenue</h2>
                <br>
                <div class="outer-box">
                    <div class="error_message">{{ $message }}</div>
                </div>
            </div>

        </div>
    </div>
</section>

</div>
<div class="wave-bottom bottom-pos"></div>
<div class="call-out">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="float_left">
          <h4>Lorem ipsum dolor sit amet dolor ipsum sit amet nec mergitur</h4>
      </div>
      <div class="float_right">
          <a href="contact.html" class="thm-btn-tr">Je souhaite m'inscrire</a>
      </div>
  </div>
</div>

</div>
</div>

@stop

