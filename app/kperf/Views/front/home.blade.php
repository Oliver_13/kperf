@extends('front.layout')
@section('page_id', 'homepage')
@section('content')

    <div class="tp-banner-container">
        <div class="tp-banner fullscreen">
            <ul>
                @foreach($data->slider as $slide)
                    <li data-transition="random-static" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
                        <div class="pattern-slider"></div>
                        <img alt="slidebg1" src="{!! asset('upload/images/'.$slide->mediaContent[0]['content']) !!}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <div class=" uppercase tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="0" data-y="center" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="530" data-start="700" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <h1 class="undertitle">
                                <span class="theme-color">{{ $slide['content']['titre'] }}</span><br />
                                {{ $slide['content']['sous_titre'] }}
                            </h1>
                        </div>
                        <div class="undertitle tp-caption skewfromleft customout rs-parallaxlevel-0" data-x="0" data-y="340" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="530" data-start="900" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1">
                            <p class="app undertitle hide-on-m" style="text-transformation:lowercase"> {{ $slide['content']['intro'] }}</p>
                        </div>
                    </li>
                 @endforeach
            </ul>
        </div>
    </div>

	<div class="dima-main">

        <section class="section">
            <div class="page-section-content overflow-hidden">
                <div class="container text-center">
                    
                    <h2 class="uppercase" data-animate="fadeInDown" data-delay="0">{{ $data['titre'] }}</h2>
                    <div class="topaz-line">
                        <i class="di-separator"></i>
                    </div>
                    {!! $data['texte'] !!}

                </div>
            </div>
        </section>

        <section class="section  section-colored" data-bg="#faf9f5" id="features">
            <div class="page-section-content overflow-hidden">
                <div class="container-fluid text-center">
                    
                    <h2 class="uppercase" data-animate="fadeInDown" data-delay="0">{{ $data->pageCompetence->content['titre'] }}</h2>
                    <div class="topaz-line" data-animate="fadeIn" data-delay="0">
                        <i class="di-separator"></i>
                    </div>
                    <div class="clear-section"></div>
                    
                    <div class="ok-row">

                        @foreach($data->competences as $competence)
                            <div class="ok-md-2 ok-xsd-6" data-animate="fadeInUp" data-delay="0">
                                <div class="no-box features-start">
                                    <header role="banner">
                                        <img src="{!! asset('upload/images/'.$competence->mediaContent[0]['content']) !!}" style="max-width:150px" class="img-responsive" />
                                    </header>
                                    <div class="features-content">
                                        <h5 class="uppercase features-title">{{ $competence->content['titre'] }}</h5>
                                    </div>
                                </div>
                            </div>
                            @if($loop->iteration == 6) <div class="clear-section"></div> @endif
                        @endforeach
                        <div class="clear-section"></div>
                    </div>

                </div>
            </div>
        </section>
		
		<section class="section">
            <div class="page-section-content overflow-hidden">
                <div class="container explain text-center">
                    
                    <h2 class="uppercase" data-animate="fadeInDown" data-delay="0">{{ $data->pagePriseEnCharge->content->titre }}</h2>
                    <div class="topaz-line" data-animate="fadeIn" data-delay="0">
                        <i class="di-separator"></i>
                    </div>
                    <div class="clear-section"></div>
                    
                    @foreach($data->etapes as $etape)
                         <div class="ok-row">
                            @if($loop->iteration  % 2 == 0)

                                <div class=" ok-md-6 ok-xsd-12" data-animate="fadeInLeft" data-delay="0">
                                    <img src="{!! asset('upload/images/'.$etape->mediaContent[0]['content']) !!}" alt="{{ $etape->content['titre'] }}">
                                </div>
                                <div class=" ok-md-6 ok-xsd-12 extx text-right" data-animate="fadeInRight" data-delay="0">
                                    <h3>{{ $etape->content['titre'] }}</h3>
                                    {{ $etape->content['intro'] }}
                                </div>

                            @else

                                <div class=" ok-md-6 ok-xsd-12 extx text-left" data-animate="fadeInLeft" data-delay="0">
                                    <h3>{{ $etape->content['titre'] }}</h3>
                                    {{ $etape->content['intro'] }}
                                </div>
                                <div class=" ok-md-6 ok-xsd-12 " data-animate="fadeInRight" data-delay="0">
                                    <img src="{!! asset('upload/images/'.$etape->mediaContent[0]['content']) !!}" alt="{{ $etape->content['titre'] }}">
                                </div>
                             
                            @endif
                        </div>
                    @endforeach
					
                </div>
            </div>
        </section>
		
        <section class="section" id="quote">
            <div class="page-section-content overflow-hidden">
                <div class="background-image-hide parallax-background">
                    <img class="background-image" alt="Background Image" src="{!! asset('kperf/images/sections/quote-bg.jpg') !!}">
                </div>
                <div class="dima-section-cover"></div>
                <div class="container page-section text-center" data-animate="bounceIn" data-delay="0">
                    <div class="ok-md-12 ok-xsd-12" data-animate="bounceIn" data-delay="0">
                        <div class="owl-carousel">

                            @foreach($data->blockquotes as $blockquote)
                                @if($loop->count == 1)
                                    <div class="dima-testimonial quote-style">
                                        <blockquote>
                                            {!! $blockquote->content['intro'] !!}
                                        </blockquote>
                                    </div>
                                    <div class="dima-testimonial quote-style">
                                        <blockquote>
                                            {!! $blockquote->content['intro'] !!}
                                        </blockquote>
                                    </div>
                                @else
                                    <div class="dima-testimonial quote-style">
                                        <blockquote>
                                            {!! $blockquote->content['intro'] !!}
                                        </blockquote>
                                    </div>
                                @endif
                            @endforeach

                        </div>
                    </div>
                    <div class="ok-md-2"></div>
                </div>
            </div>
        </section>

        @if(!is_null($data->pageMission))

            @if(!is_null($data->pageMission->mediaContent[0]['content']))
                <section class="section">
                    <div class="page-section-content overflow-hidden">
                        <div class="container text-center">
                            <p data-animate="fadeInUp" data-delay="100">
                                <img src="{!! asset('upload/images/'.$data->pageMission->mediaContent[0]['content']) !!}" style="width:800px" class="img-responsive" />
                            </p>
                        </div>
                    </div>
                </section>
            @endif

            @if(!is_null($data->pageMission->content->intro))
                <section class="section  section-colored" data-bg="#faf9f5">
                    <div class="page-section-content overflow-hidden">
                        <div class="container text-center">
                            {!! $data->pageMission->content->intro !!}
                        </div>
                    </div>
                </section>
            @endif

        @endif

    </div>

    @push('js')
		<script type="text/javascript" src="{!! asset($project.'/js/specific/revolution-slider/js/jquery.themepunch.tools.min.js') !!}"></script>
		<script type="text/javascript" src="{!! asset($project.'/js/specific/revolution-slider/js/jquery.themepunch.revolution.min.js') !!}"></script>
    @endpush

@stop
