<?php

namespace App\kperf\Models;

use App;
use Illuminate\Support\Str;
use App\Wagaia\Cms\Models\Nav;
use App\Wagaia\Cms\Models\Pages;
use App\Wagaia\Cms\Models\PagesData;
use App\Wagaia\Cms\Helpers\Helpers;
use Illuminate\Database\Eloquent\Model;


class Page extends Model
{
	use \App\Wagaia\Core\Traits\Helper {
		\App\Wagaia\Core\Traits\Helper::__construct as private Helper__construct;
	}

	public $timestamps = false;
    protected $nav;
    protected $metaData;

	public function __construct()
    {
        $this->Helper__construct();
        $this->nav = (new Nav)->buildNav();
        $this->metaData = Pages::where('type','meta_data')->with('children')->first();

    }

    public function show()
    {

        $requested = request()->segments();

        if(empty($requested) or (current($requested) == App::getlocale() && count($requested)<2)) {
            return $this->homePage();
        }

        $url = end($requested);

        $q = $data = PagesData::where([
            'nav_url' => $url,
            'lg' => App::getlocale()
        ]);

        $data = PagesData::where([
            'nav_url' => $url,
            'lg' => App::getlocale()
        ])->first();


        $view = !is_null($data) && ($data->page->type &&
        view()->exists('front.'.$data->page->type)) ?
        $data->page->type : 'page';

        if ($view == 'home') {
            return $this->homePage();
        }

        return view('front.'.$view)->with([
                'nav'=> $this->nav,
                'data'=> $data,
                'metaData'=> $this->metaData
            ]);

    }

    protected function homePage()
    {
        return view('front.home')->with([
                'nav'=> $this->nav,
                'data'=>Pages::where('type','home')->with('children')->first(),
                'metaData'=> $this->metaData
            ]);
    }

}