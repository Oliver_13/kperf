<?php

namespace App\kperf\Models;

use \App\Wagaia\Cms\Models\Pages;
use Illuminate\Database\Eloquent\Model;


class Media extends Model
{

	public $timestamps = false;

    public function mediaKey(Pages $page)
    {

        if (!is_null($page)) {

            switch($page->type) {
                case 'media':
                case 'actus':
                case 'evenements':
                case 'subpage':
                return $page->hasParent->hasParent->access_key;
                break;

                case 'subpage_child':
                return $page->hasParent->hasParent->hasParent->access_key;
                break;

                case 'section_presentation':
                return $page->hasParent->access_key;
                break;

                default :
                return $page->access_key;
            }

        }
    }

}