<?php

namespace App\kperf\Models;

use Mail;
use App\Wagaia\Cms\Helpers\Helpers;
use Illuminate\Database\Eloquent\Model;


class Mails extends Model
{
	use \App\Wagaia\Core\Traits\Helper {
        \App\Wagaia\Core\Traits\Helper::__construct as private Helper__construct;
    }

    public $timestamps = false;
    public $table = "wagaia_mails";
    public $contact_mail;

    public function __construct()
    {
        $this->Helper__construct();
        $this->contact_mail = config('mail.from.address');
    }

    protected function toDatabase()
    {
        $data = [];
        foreach($this->checkable as $v) {
            $data[$v] = request()->{$v};
        }

        // Consent
        $consent = request()->has('form_terms') ? date('Y-m-d H:i:s') : null;
        if ($consent) {
            $data['key'] = str_random(20);
        }
        self::insert($data);

        // Reset consent
        self::where('form_email', $_POST['form_email'])->update(['consent'=>$consent]);
    }

    public function contact()
    {
        $this->checkable = ['form_name', 'form_subject', 'form_email', 'form_message', 'form_phone'];

        $this->validation_rules = [
            'form_phone'=>'required',
            'form_name'=>'required',
            'form_subject'=>'required',
            'form_email'=>'required',
            'form_message'=>'required'
        ];

        $this->validation_messages = [
            'form_email.required'  => "L'adresse email est obligatoire",
            'form_message.required'=> "Veuillez écrire votre message",
            'form_name.required'=> "Veuillez indiquer votre prénom / nom",
            'form_phone.required'=> "Veuillez indiquer un numéro de téléphone",
            'form_phone.subject'=> "Veuillez préciser le sujet",
        ];

        $this->validate();

        foreach($this->checkable as $v) {
            if (!request()->filled($v)) {
                $this->response['error'][] = trans('wagaia.'.str_replace('_','.',$v)) . trans('wagaia.is_mandatory_information');
            }
        }

        if ($this->noErrors()) {

            $this->toDatabase();
            Mail::to($this->contact_mail)
                    ->subject('Demande de contact')
                    ->send(new \App\kperf\Mail\Contact());

            $this->response['message'] = "Merci ! Votre message a été envoyé.";
        }

        return $this->response;
    }

}