<?php

namespace App\kperf\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ForumMessage extends Model
{

    public $table = 'wagaia_forum_messages';

    use \App\Wagaia\Core\Traits\Helper {
        \App\Wagaia\Core\Traits\Helper::__construct as private Helper__construct;
    }

    public function __construct()
    {
        $this->Helper__construct();
    }

    protected function create_message()
    {
        $this->editable = new self;
        $this->editable->forum_id = session()->get('forum_id');
        $this->editable->user_id = 1;
        $this->editable->message = request()->topic_message;
        $this->editable->save();

        $this->response['topic'] = $this->editable->toArray();
        $this->push_success_response("Le message a été enregisitré");

        return $this->response;

    }

    public function author()
    {
        return $this->belongsTo(User::class,'user_id')->select('id','name','email');
    }

}