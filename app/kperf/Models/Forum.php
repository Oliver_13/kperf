<?php

namespace App\kperf\Models;

use Auth;
use App\User;
use App\Wagaia\Cms\Models\Pages;
use Illuminate\Database\Eloquent\Model;


class Forum extends Model
{

    public $table = 'wagaia_forum';
    //protected $fillable = ['pages_id','topic_title','topic_message','user_id','visibility','nav_url'];

    use \App\Wagaia\Core\Traits\Helper {
        \App\Wagaia\Core\Traits\Helper::__construct as private Helper__construct;
    }

    public function __construct()
    {
        $this->Helper__construct();
    }

    public function index($visibility=null)
    {
        //return self::visibility($visibility)->orderBy('updated_at', 'desc')->withCount('messages')->get();
    }

    public static function getTopics($section_id, $visibility=null)
    {
        return self::where('pages_id', $section_id)->visibility($visibility)->orderBy('updated_at', 'desc')->withCount('messages')->with('author')->get();
    }

    public static function getTopic(string $url)
    {
        $visibility = null;
        if (Auth::check()) {

        }
        return self::where('nav_url', $url)->visibility($visibility)->withCount('messages')->with(['author','messages'])->first();
    }

    protected function create_topic()
    {
        // validate : session pages_id / : user_id
        $pages_id = session()->get('section_id');
        if (!is_int($pages_id)) {
            $this->push_error_response("La section n'est pas identifiable");
            return $this->response;
        }

        $this->editable = new self;
        $this->editable->pages_id = $pages_id;
        $this->editable->user_id = 1;
        $this->editable->visibility = request()->visibility;
        $this->editable->topic_title = request()->topic_title;
        $this->editable->topic_message = request()->topic_message;
        $this->editable->save();

        $this->editable->nav_url = $this->editable->id.'-'.str_slug(request()->topic_title); // session()->get('current_url').'/'.
        $this->editable->save();

        $this->response['topic'] = $this->editable->toArray();
        $this->response['topic']['url'] = session()->get('current_url').'/';
        $this->response['callback'] = request()->callback;

        $this->push_success_response("Le fil de discussion a été créé");

        return $this->response;
    }

    public function scopeVisibility($query, $visibility)
    {
        if ($visibility) {
            return $query->where('visibility', $visibility);
        }
    }

    public function messages()
    {
        return $this->hasMany(ForumMessage::class)->orderBy('updated_at');
    }

    public function author()
    {
        return $this->belongsTo(User::class,'user_id')->select('id','name','email');
    }


}