<?php

namespace App\kperf\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\kperf\Mail\Contact;

class ContactController extends Controller
{
	public function store(Request $request) {

		$messages = [
			'name.required' => 'Veuillez renseigner votre nom',
			'email.required' => 'Veuillez renseigner votre adresse email',
			'subject.required' => 'Veuillez renseigner un sujet',
			'message.required' => 'Veuillez renseigner votre message',
		];

		$successMessage = "Vote demande a bien été prise en compte";

	    Validator::make($request->all(), [
	        'name' => 'required',
	        'email' => 'required',
	        'subject' => 'required',
	        'message' => 'required',
	    ], $messages)->validate();


		Mail::to('obrutinel@wagaia.com')
            ->send(new Contact($request->except('_token')));


		return redirect('contact')->with('message', $successMessage);

	}

}