<?php

namespace App\kperf\Controllers;

use App;
use Pages;
use Route;
use App\Http\Controllers\Controller;
use App\Wagaia\Cms\Models\PagesData;
use App\Wagaia\Cms\Models\Nav;
use App\kperf\Helpers\Helpers;

class FrontController extends Controller
{
    public $locale, $data, $nav;
    protected static $error;

    public function __construct() {

        $this->locale = App::getLocale();
    }

    public static function routes()
    {
        Route::any('ajax', '\App\kperf\Controllers\FrontController@ajax');
        Route::post('contact', '\App\kperf\Controllers\ContactController@store');
        Route::any('{all}', '\App\kperf\Controllers\FrontController@distribute')->where('all', '.*');
    }

    public function ajax()
    {
        $this->object = ucfirst(request()->ajax_object);
        $called_class = '\App\kperf\\Models\\'.$this->object;

        if (!class_exists($called_class)) {
            return response()->json('error', "Cette ajax requête ne peut pas aboutir");
        }

        return (new $called_class)->ajax();
    }

    public function distribute()
    {

        $this->nav = (new Nav)->buildNav();

        $url = request()->segments();
        $this->data = PagesData::where('nav_url', end($url))->first();

        if (is_null($this->data)) {
            return abort(404, 'La page n\'existe pas');
        }
        else {

            $functionName = Helpers::toCamelCase($this->data->page->type);

            if(method_exists($this, $functionName)) {
                return $this->{$functionName}();
            }
            else {
                return view('front.'.$this->data->page->type)->with(array(
                    'data' => $this->data,
                    'nav' => $this->nav
                ));
            }

        }

    }

    protected function home() {

        $this->data->slider = $this->data->page->children->where('type','list-slide')->first()->children->where('published', 1);

        $this->data->pageCompetence = Pages::where('type', 'list-competence')->with('content')->first();
        $this->data->competences = $this->data->pageCompetence->children->where('published', 1);

        $this->data->pagePriseEnCharge = Pages::where('type', 'list-etape')->with('content')->first();
        $this->data->etapes = $this->data->pagePriseEnCharge->children->where('published', 1);

        $this->data->blockquotes = Pages::where('type','list-blockquote')->first()->children->where('published', 1);

        $this->data->pageMission = Pages::where(['type' => 'mission', 'published' => 1])->first();

        return view('front.home')->with(array('data' => $this->data, 'nav' => $this->nav));

    }

}
