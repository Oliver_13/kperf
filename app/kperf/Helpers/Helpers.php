<?php

namespace App\kperf\Helpers;

use App\Wagaia\Cms\Models\PagesData;

class Helpers
{

    public function __construct()
    {
        $this->locale = app()->getLocale();
        $this->locales = config('app.locales');
        $this->object_id = session()->get('object_id');
    }


    /**
    * Créé un lien depuis une page_id
	* @var $page_id string|int
	* @var $full boolean
	* @var $class string
	* @return string 
    */
    public static function getLink($page_id, $full = false, $class = null) {

		$output = '';

    	if(!empty(preg_match('/^0*[1-9]\d*$/', $page_id))) {

    		$p = PagesData::find($page_id);

	    	if($full) {
	    		$output = '<a href="'.$p->nav_url.'" '.(!empty($class)?'class="'.$class.'"':'').'>'.$p->nav_title.'</a>';
	    	}
	    	else $output = $p->nav_url;

   		}

        return $output;

    }

    /**
     * Convertie une chaine de caractere en camelCase
     * @param $str
     * @param array $noStrip
     * @return mixed|null|string|string[]
     */
    public static function toCamelCase($str, array $noStrip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);

        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        $str = lcfirst($str);

        return $str;
    }

}