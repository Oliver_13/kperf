<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'fr_FR');

        Schema::defaultStringLength(191);

        View::share('locales', config('app.locales'));
        View::share('project', config('app.project'));
        View::share('call_fileupload', false);
        View::share('is_multilang', config('app.is_multilang'));

        App::bind('pages', function() {
            return new \App\Wagaia\Cms\Models\Pages;
        });
        App::bind('media', function() {
            return new \App\Wagaia\Cms\Models\MediaManager;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
