<?php

Auth::routes();
Route::post('logout', function() {
    Auth::logout();
    return redirect()->route('login');
});

Route::get('/config-cache', function() {
	$exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
	$exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    return '<h1>Clear Config cleared</h1>';
});


# PANEL
\App\Wagaia\Cms\Controllers\PanelController::routes();

// ACCES COMPTE CLIENT
// ---------------------
//\App\Wagaia\Cms\Controllers\AccountController::routes();


# FRONT
\App\kperf\Controllers\FrontController::routes();
